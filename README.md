# holoLogs: your own personal Timeline Logs

### as of 09/08/20 23:06:43

* total commits : 2583
* total hours worked : 2736.05 hours
* total KIND created : 15498.0 KINDs

![kat][1]

[1]: kau_energy.png


## time monitoring :

use *.lof for activity analysis...


### Syntropy measurment :

use git log for value computation

commits are counted as 6 KIND unit

time unit is the tomato : 1769 sec.

Work hours: are 8h / day for 5 days per week

work time is counted per period of 8 tomatos : 14152 sec.
<br>if there is a commit in a time period the period is counted as worked

The total worked time is use to compute the tomato per commit average (tpc=2.15563298490125)
such that the KIND unit is scaled to the attention given to the project

1 kau ~ 10.593 minutes

## scripts

[listrepo.sh](bin/listrepo.sh): dash script for listing all present GIT repositories
[runit.sh](bin/runit.sh): top script to run to re-evalutate
[process.pl](bin/process.pl): perl script to compute time and energy created




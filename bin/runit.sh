#

dir=${0%/*}
tic=$(date +%s)
mtime=$(stat -c %Y runit.log)
cd $dir/..
echo "--- # ${0##*/}"
if test ! -e runit.log || expr $tic \- 3600 \> $mtime 1>/dev/null; then
xargs -L 1 -a _data/git.lof sh bin/logdates.sh | tee runit.log
fi
if grep -q git: runit.log; then
grep git: runit.log > alldates.yml
fi
perl bin/process.pl runit.log | tee process.log
eval $(cat process.log | eyml)
date=$(date +"%D %T")
sed -e "s,%date%,$date," -e "s/%tc%/$tc/g" -e "s/%tot%/$tot/g" \
 -e "s/%tpc%/$tpc/" -e "s/%kpc%/$kpc/" -e "s/%kau%/$kau/" \
 -e "s/%vat%/$vat/g" -e "s/%kat%/$kat/g" process_tmpl.kst > process.kst

sed -e "s,%date%,$date," -e "s/%tc%/$tc/g" -e "s/%tot%/$tot/g" \
 -e "s/%tpc%/$tpc/" -e "s/%kpc%/$kpc/" -e "s/%kau%/$kau/" \
 -e "s/%vat%/$vat/g" -e "s/%kat%/$kat/g" README.tmpl > README.md
kst2 --png kau.png process.kst

git add process.kst README.md
qm=$(ipfs add -Q -w process.log kau_energy.png README.md *.csv process.kst)
echo $tic: $tc $qm >> qm.log

#!/usr/bin/perl

local *S; open S,'>','skip.csv';
print S "#sn,repoid,commitid,type,desc,tics,date,repo,treeid,email,name,parents,subject,note,body\n";
local *M; open M,'>','michel.csv';
print M "#sn,repoid,commitid,type,desc,tics,date,repo,treeid,email,name,parents,subject,note,body\n";
local *P; open P,'>','processed.csv';
print P "#sn,repoid,commitid,type,desc,tics,date,repo,treeid,email,name,parents,subject,note,body\n";

my $t0 = undef;
my %value = ();
my @value = ();
my %commit = ();
my @commit = ();
my %seen = ();
my $tomato = 29 * 61; # in sec
my $period = 8 * $tomato;
my $i = 0;
my $p = 0;
while (<>) {
   my $top = $1 if (m/top: (.*)/);
   if (m/^[0-9a-f]+,/) {
      my ($commitid,$type,$desc,$tic,$date,$repo,$treeid,$email,$name,$parents,$subject,$note,$body) = split(',',$_);
      next if ($tic < 804000000);
      if (! $seen{$commitid}) { $seen{$commitid} = $i++; }
      if (! $seen{$repo}) { $seen{$repo} = $p++; }
      my $skip = 0;
      $skip++ if ($seen{$repo} == 180 ); # universalviewer
      $skip++ if ($seen{$repo} == 181 ); # git-book-convert
      $skip++ if ($seen{$repo} == 183 ); # rel
      #$skip++ if ($seen{$repo} == 185 ); # etherpad-lite
      $skip++ if ($seen{$repo} == 186 ); # brig
      $skip++ if ($seen{$repo} == 195 ); # isomorphic-git
      $skip++ if ($seen{$repo} == 197 ); # kiko-now
      $skip++ if ($seen{$repo} == 200 );
      $skip++ if ($seen{$repo} == 201 ); # idena-go
      $skip++ if ($seen{$repo} == 130 ); # gnome-colors
      $skip++ if ($seen{$repo} == 141 ); # darknet
      if ($email =~ /\.(gq|ml|tk|cf)/ || $name =~ m/michel\b/i || $name =~ m/(?:iggy|emile)/i || $email =~ m/gitea/) {
        print M $seen{$commitid},',',$seen{$repo},',',$_;
        my $ttime = int($tic / $tomato);
        my $vtime = int($tic / $period);
        $commit{$ttime}++; # account for commits during a tomato period...
        $value{$vtime}++; # account for value ...
      } elsif($skip) {
        print S $seen{$commitid},',',$seen{$repo},',',$_;
      } else {
        print P $seen{$commitid},',',$seen{$repo},',',$_;
      }
   }
}


local *V; open V,'>','value.csv';
print V "#tic,wtime,value,attention\n";
my $vat = 0;
my $rate = 8/24 * 5/7;
my $t0 = undef;
foreach my $vt (sort { $a <=> $b } keys %value) {
   my $tic = $vt * $period;
   $t0 = $tic if (! defined $t0);
   $vat += ($value{$vt} > 0) ? $period / 3600 : 0;
   my $work = ($tic - $t0) * $rate / 3600;
   printf V "%u,%d, %d,%d\n",$tic,$work, $value{$vt},$vat;
}

my $tc = 0;
foreach my $tt (keys %commit) {
 $tc += $commit{$tt};
}
printf "tc: %u\n",$tc;
my $tpc = $vat * 3600 / $tc / $tomato;  # tomato per commit
printf "tot: %.1f\n",$tc * $tpc;
printf "tpc: %s\n",$tpc;
printf "kpc: ~%s\n",$tpc * $tomato / (12*60); # ~ 12 minutes
my $kpc = int($tpc * $tomato / (12*60) + 0.999);
printf "kpc: %s\n",$kpc;
# attention time : 6 = tpc * 29*61 / (kat * 60)
# kau = tpc * 29*61 / (kpc * 60)
my $kau = $tpc * $tomato / ($kpc*60); # Krystal Attention Unit
printf "kau: %.3f\n",$kau;

local *T; open T,'>','tomato.csv';
print T "#tic,syn,tomato,kattention\n";
my $kat = 0;
my $rate = 8/24 * 5/7;
my $t0 = undef;
foreach my $tt (sort { $a <=> $b } keys %commit) {
   my $tic = $tt * $tomato;
   $t0 = $tic if (! defined $t0);
   $kat += $commit{$tt} * $kpc; # one kind ~ 10 minutes
   my $syn = ($tic - $t0) * $rate / ($kau*60);
   printf T "%u,%d, %d,%d\n",$tic,$syn, $commit{$tt},$kat;
}


printf "vat: %.2f\n",$vat;
printf "kat: %.1f\n",$kat;

close M,S,P,V,T;

exit $?;
